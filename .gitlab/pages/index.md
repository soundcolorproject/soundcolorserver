
# Sound Color Project
* [Coverage Report](./coverage)
* [Cypress Report](./cypress-results)
* [Cypress Dashboard](https://dashboard.cypress.io/projects/68725n/runs)
* [Storybook](https://soundcolorproject.com/storybook/)
* [Staging environment](https://soundcolor-stg.herokuapp.com/)
* [Production environment](https://soundcolorproject.com/)
