image: node:lts

stages:
  - install
  - check
  - postbuild
  - deploy

variables:
  npm_config_cache: $CI_PROJECT_DIR/.cache/npm
  CYPRESS_CACHE_FOLDER: $CI_PROJECT_DIR/.cache/Cypress
  BUILD_DIR: dist
  NODE_ENV: production
  HEROKU_STG: https://git.heroku.com/soundcolor-stg.git
  HEROKU_PROD: https://git.heroku.com/soundcolor.git

install:
  stage: install
  cache:
    key: $CI_COMMIT_REF_NAME NPM_CACHE
    paths:
      - .cache/npm
  script:
    - NODE_ENV='' npm ci
  artifacts:
    expire_in: 1 day
    paths:
      - node_modules

run-tslint:
  stage: check
  dependencies:
    - install
  script:
    - npm run lint:ts

run-glslLint:
  stage: check
  dependencies:
    - install
  script:
    - npm run lint:glsl

run-tests:
  stage: check
  dependencies:
    - install
  variables:
    NODE_ENV: ''
  script:
    - npm run build:css-types
    - npm test
  artifacts:
    expire_in: 1 week
    paths:
      - coverage

build-app:
  stage: check
  dependencies:
    - install
  variables:
    NODE_ENV: production
    USE_REMOTE_API: 'true'
  script:
    - npm run full-build
  artifacts:
    expire_in: 1 day
    paths:
      - dist

run-cypress:
  image: cypress/browsers:node12.18.0-chrome83-ff77
  stage: postbuild
  dependencies:
    - install
    - build-app
  cache:
    key: $CI_COMMIT_REF_NAME CYPRESS_CACHE
    paths:
      - .cache/Cypress
  script:
    - npx cypress install
    - npx cypress verify
    - npm run cy:ci
  artifacts:
    expire_in: 1 week
    paths:
      - cypress/videos
      - cypress/screenshots
      - cypress/report

# unit test coverage report will be put into the gitlab pages for the project
pages:
  stage: deploy
  dependencies:
    - install
    - run-tests
    - run-cypress
  script:
    - npm i -g showdown
    - rm -rf public
    - ./scripts/convertMarkdown.sh public
    - mv coverage/lcov-report public/coverage
    - mv cypress/report public/cypress-results
  artifacts:
    expire_in: 1 day
    paths:
      - public
  only:
    refs:
      - master

.deploy: &deploy
  stage: deploy
  dependencies:
    - build-app
  script:
    # The quotes are required here in order to be able to escape the newline characters
    - "echo \"machine git.heroku.com\n  login $HEROKU_API_USER\n  password $HEROKU_API_KEY\" > ~/.netrc"
    - ./scripts/clean-deps.js
    - rm -rf .git
    - git init
    - git config user.email "$HEROKU_API_USER"
    - git config user.name "GitLab CI"
    - git add .
    - git add dist -f
    - git commit -m "GitLab Build"
    - git remote add heroku "$heroku_app"
    - git push heroku master -f

deploy-stg:
  <<: *deploy
  variables:
    heroku_app: $HEROKU_STG
  environment:
    name: Staging
    url: https://soundcolor-stg.herokuapp.com/
  only:
    refs:
      - /^stg\/.*$/

deploy-prod:
  <<: *deploy
  variables:
    heroku_app: $HEROKU_PROD
  environment:
    name: Production
    url: https://soundcolorproject.com/
  only:
    refs:
      - master
